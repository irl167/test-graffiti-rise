import os
import xarray as xr
import warnings
warnings.filterwarnings('ignore')
from matplotlib import cm, pyplot as plt
# from mpl_toolkits.basemap import Basemap

import numpy as np
from matplotlib import gridspec
from matplotlib.colors import DivergingNorm
import sys
from scipy import stats, interpolate, signal

import math
from matplotlib.collections import RegularPolyCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable

import cartopy
import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
from scipy import stats

#%% 


############################################  CALCULATIONS ############################################

def shoot(lon, lat, azimuth, maxdist=None):
    """Shooter Function
    Original javascript on http://williams.best.vwh.net/gccalc.htm
    Translated to python by Thomas Lecocq
    """
    glat1 = lat * np.pi / 180.
    glon1 = lon * np.pi / 180.
    s = maxdist / 1.852
    faz = azimuth * np.pi / 180.

    EPS= 0.00000000005
    if ((np.abs(np.cos(glat1))<EPS) and not (np.abs(np.sin(faz))<EPS)):
        print("Only N-S courses are meaningful, starting at a pole!")

    a=6378.13/1.852
    f=1/298.257223563
    r = 1 - f
    tu = r * np.tan(glat1)
    sf = np.sin(faz)
    cf = np.cos(faz)
    if (cf==0):
        b=0.
    else:
        b=2. * np.arctan2 (tu, cf)

    cu = 1. / np.sqrt(1 + tu * tu)
    su = tu * cu
    sa = cu * sf
    c2a = 1 - sa * sa
    x = 1. + np.sqrt(1. + c2a * (1. / (r * r) - 1.))
    x = (x - 2.) / x
    c = 1. - x
    c = (x * x / 4. + 1.) / c
    d = (0.375 * x * x - 1.) * x
    tu = s / (r * a * c)
    y = tu
    c = y + 1
    while (np.abs (y - c) > EPS):

        sy = np.sin(y)
        cy = np.cos(y)
        cz = np.cos(b + y)
        e = 2. * cz * cz - 1.
        c = y
        x = e * cy
        y = e + e - 1.
        y = (((sy * sy * 4. - 3.) * y * cz * d / 6. + x) *
              d / 4. - cz) * sy * d + tu

    b = cu * cy * cf - su * sy
    c = r * np.sqrt(sa * sa + b * b)
    d = su * cy + cu * sy * cf
    glat2 = (np.arctan2(d, c) + np.pi) % (2*np.pi) - np.pi
    c = cu * cy - su * sy * cf
    x = np.arctan2(sy * sf, c)
    c = ((-3. * c2a + 4.) * f + 4.) * c2a * f / 16.
    d = ((e * cy * c + cz) * sy * c + y) * sa
    glon2 = ((glon1 + x - (1. - c) * d * f + np.pi) % (2*np.pi)) - np.pi	

    baz = (np.arctan2(sa, b) + np.pi) % (2 * np.pi)

    glon2 *= 180./np.pi
    glat2 *= 180./np.pi
    baz *= 180./np.pi

    return (glon2, glat2, baz)
#%%


    
############################################  PLOTTING ############################################    
    
    
    
def Plot_track(TCs_h, figsize=[25,12], cmap='magma', m_size=80):
    
    fig = plt.figure(figsize=figsize)

    ax = plt.axes(projection = ccrs.PlateCarree(central_longitude=190))
    ax.stock_img()

    # cartopy land feature
    land_110m = cartopy.feature.NaturalEarthFeature('physical', 'land', '110m', edgecolor='darkgrey', facecolor='gainsboro',  zorder=1)
    ax.add_feature(land_110m)
    ax.gridlines()
    
    ax.plot(TCs_h.lon.values, TCs_h.lat.values, '-', color='grey', transform=ccrs.PlateCarree())
    
    s1=ax.scatter(TCs_h.lon.values, TCs_h.lat.values, m_size, TCs_h.wmo_pres.values, transform=ccrs.PlateCarree(), zorder=3, cmap=cmap)
    
    ax.plot(TCs_h.lon.values[0], TCs_h.lat.values[0], 's', color='navy', markersize=10, transform=ccrs.PlateCarree(), zorder=5)
    
    ax.text(TCs_h.lon.values[0]+0.8, TCs_h.lat.values[0]+0.8, str(TCs_h.name.values) + ' (' + str(TCs_h.time_origin.dt.year.values) + ')' , 
            color='navy', fontsize=13, fontweight='bold', transform=ccrs.PlateCarree())
    ax.set_extent([np.nanmin(TCs_h.lon.values)-15, np.nanmax(TCs_h.lon.values)+15, np.nanmin(TCs_h.lat.values)-15, np.nanmax(TCs_h.lat.values)+15], crs=ccrs.PlateCarree())

    plt.colorbar(s1, pad=0.01).set_label('Pressure (mbar)', fontsize=15)
    
    return fig, ax


def Generate_Waves_TC(tc, som_fit, TCs_waves, min_number_data=20, prct_plot=0.75, prctiles=np.arange(0,1.05,0.025), 
                   plotting=1, interp=0, interp_factor=1, min_points=5, hmin=1, hmax=8):
    
    '''
    
    min_number_data: Remove results when data for the fitting was less than min_number_data
    prct_plot: Percentile to choose for plotting
    prctiles: Percentiles to calculate 
    plotting: 1:activate /0:deactivate 
    interp: 1:activate / 0:deactivate 
    interp_factor: if interp=1, multiply by interp_factor the number of points along the track
    min_points: minimum of track points to obtain associated waves
    [hmin, hmax]: range for Hs plotting
    
    '''
        
    angles=np.r_[TCs_waves.angle.values,360]
    distance=np.r_[TCs_waves.distance.values,500]
    max_pressure=1000 #Remove tc points with a pressure larger than 1000mbar
    
    
    tc=tc.isel(date_time=(~np.isnan(tc.wmo_pres)))
    
    if len(tc.date_time)>min_points:
        
        tc=tc.isel(date_time=np.where(tc.wmo_pres.values<=max_pressure)[0])
        
    if len(tc.date_time)>min_points:
        
        lons=tc.lon.values; 
        lons[np.where(lons<0)[0]]=lons[np.where(lons<0)[0]]+360
        tc=tc.drop('lon')
        tc['lon']=lons
        
        if interp==1:
            
            total_points=len(tc.date_time)*interp_factor
            time_new=np.linspace(tc.date_time.values[0],tc.date_time.values[-1],total_points)
            f= interpolate.interp1d(tc.date_time.values,tc.lon.values)
            lonnew=f(time_new)
            f= interpolate.interp1d(tc.date_time.values,tc.lat.values); latnew=f(time_new)
            f= interpolate.interp1d(tc.date_time.values,tc.lat.values); latnew=f(time_new)
            f= interpolate.interp1d(tc.date_time.values,tc.wmo_pres.values); pressnew=f(time_new)
            f= interpolate.interp1d(tc.date_time.values,tc.storm_speed.values); speednew=f(time_new)
            f= interpolate.interp1d(tc.date_time.values,tc.storm_dir.values); dirnew=f(time_new)
            
            tc=xr.Dataset(
                    {   'time': (('date_time'), time_new),
                        'lon': (('date_time'), lonnew),
                        'lat': (('date_time'), latnew),
                        'wmo_pres': (('date_time'), pressnew),
                        'storm_speed': (('date_time'), speednew),
                        'storm_dir': (('date_time'), dirnew),
                        'name' : (tc.name),
                        'time_origin' : (tc.time_origin), },
                    {   'date_time': signal.resample(tc.date_time,total_points),}
                )
            
        
        LON2=np.full([len(distance),len(angles),len(tc.date_time)],np.nan)
        LAT2=np.full([len(distance),len(angles),len(tc.date_time)],np.nan)
        HS2=np.full([len(distance),len(angles),len(tc.date_time),len(prctiles)],np.nan)
        NUM_DATA=np.full([len(distance),len(angles),len(tc.date_time)],np.nan)           
        
        if plotting==1:
            
            fig, ax = Plot_track(tc, figsize=[25,12], cmap='turbo_r')
            
        for p in range(len(tc.date_time)):
            
            p_vars=[tc.wmo_pres.values[p], tc.storm_speed.values[p], np.abs(tc.lat.values[p])]
            p_vars=[tc.wmo_pres.values[p], tc.storm_speed.values[p], np.abs(tc.lat.values[p])]
            p_vars_norm=(p_vars-som_fit['mean'].values)/som_fit['std'].values
            #Som group
            bm=np.argmin(np.abs(np.sqrt((som_fit.centroids_norm.values[:,0]-p_vars_norm[0])**2
                                       +(som_fit.centroids_norm.values[:,1]-p_vars_norm[1])**2
                                       +(som_fit.centroids_norm.values[:,2]-p_vars_norm[2])**2)))
            
            #Gev
            z=np.transpose(stats.genextreme.ppf(prct_plot,TCs_waves.gev_shape_smooth[:,:,bm], TCs_waves.gev_location_smooth[:,:,bm],
                                                TCs_waves.gev_scale_smooth[:,:,bm]))
            z[np.where(np.transpose(TCs_waves.number_data_smooth.values[:,:,bm])<min_number_data)]=np.nan
            z=np.c_[z,z[:,-1]]; z=np.r_[z, [z[-1]] ]
            
            if tc.lat[p]>0:
                angle=tc.storm_dir[p].values+angles
            else:
                angle=(tc.storm_dir[p].values+360-angles)
            
            angle[angle>360]=angle[angle>360]-360
            
            distance=np.r_[TCs_waves.distance.values,500]
            
            lon2=np.full([len(distance),len(angle)],np.nan)
            lat2=np.full([len(distance),len(angle)],np.nan)
            
            for aa in range(len(angle)):
                for dd in range(len(distance)):
                    lon2[dd,aa], lat2[dd,aa], baz = shoot(tc.lon.values[p], tc.lat.values[p], angle[aa], distance[dd])
            
            lon2[lon2<180]=lon2[lon2<180]+360
            lon2[lon2>360]=lon2[lon2>360]-360
            
            if plotting==1:
                
                x1=np.c_[lon2[:,0],lon2]; x1=np.r_[ [x1[0]] ,x1 ]
                y1=np.c_[lat2[:,0],lat2]; y1=np.r_[ [y1[0]] ,y1 ]
                z1=np.c_[z[:,0],z]; z1=np.r_[ [z1[0]] ,z1 ]
                wsc=ax.pcolor(x1,y1,z1,cmap='magma_r',vmin=hmin, vmax=hmax, zorder=2, transform=ccrs.PlateCarree())
            
            LON2[:,:,p]=lon2
            LAT2[:,:,p]=lat2
            # HS2[:,:,p]=z
            
            for ap in range(len(prctiles)):
                z=np.transpose(stats.genextreme.ppf(prctiles[ap],TCs_waves.gev_shape_smooth[:,:,bm], 
                                                    TCs_waves.gev_location_smooth[:,:,bm], TCs_waves.gev_scale_smooth[:,:,bm]))
                z[np.where(np.transpose(TCs_waves.number_data_smooth.values[:,:,bm])<min_number_data)]=np.nan
                z=np.c_[z,z[:,-1]]; z=np.r_[z, [z[-1]] ]
                HS2[:,:,p,ap]=z

        n=TCs_waves.number_data_smooth[:,:,bm]
        n=np.c_[n,n[:,-1]]; n=np.r_[n, [n[-1]] ]
        NUM_DATA[:,:,p]=np.transpose(n)


        if plotting==1:

            plt.colorbar(wsc, pad=0.01).set_label('Hs (m)', fontsize=15)


        HS2[np.where(HS2>30)]=np.nan

        tc['angle']=angles
        tc['distance']=distance
        tc['percentiles']=prctiles
        tc['Hs']=(('distance', 'angle','date_time','percentiles'), HS2)
        tc['Hs_lon']=(('distance', 'angle','date_time'), LON2)
        tc['Hs_lat']=(('distance', 'angle','date_time'), LAT2)
        tc['Num_data']=(('distance', 'angle','date_time'), NUM_DATA)

    return tc



def Tc_Grid(tc, prctile, discretization, hmin=1, hmax=8, cmap='magma_r', figsize=[20,10], plotting=1):
    
    xx=np.arange(np.nanmin(tc.Hs_lon),np.nanmax(tc.Hs_lon),discretization)
    yy=np.arange(np.nanmin(tc.Hs_lat),np.nanmax(tc.Hs_lat),discretization)
    xx1,yy1=np.meshgrid(xx,yy)

    h_g=np.full([len(xx),len(yy)],np.nan)

    for axx in range(len(xx)-1):
        for ayy in range(len(yy)-1):

            s=np.where( (tc.Hs_lon.values>xx[axx]) & (tc.Hs_lon.values<xx[axx+1]) & (tc.Hs_lat.values>yy[ayy]) & (tc.Hs_lat.values<yy[ayy+1]) )
            p=np.where(tc.percentiles.values==prctile)[0]
            if len(s[0])>0:
                h_g[axx,ayy]=np.nanmax(tc.Hs.values[s[0],s[1],s[2],p])
               
    #Plotting
    
    if plotting:
        fig, ax = Plot_track(tc, figsize=figsize, cmap='turbo_r', m_size=40)
        tcsc=ax.pcolor(xx1,yy1,np.transpose(h_g),vmin=hmin, vmax=hmax, zorder=1, cmap=cmap, alpha=1,transform=ccrs.PlateCarree())
        plt.colorbar(tcsc, pad=0.01).set_label('Hs (m)', fontsize=15)
    
    
    tc_grid=xr.Dataset(
                    {   'Hs': (('lon', 'lat'), np.transpose(h_g)),
                         },
                    {   'lon': xx1[:,0],
                        'lat': yy1[0,:],
                    
                    }
                )
    
    return tc_grid
    
    
    
    
    
    
    
    
    
    
    
    
    
    

