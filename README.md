
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lauracagigal%2Ftest-graffiti-rise/master)


# Wave Footprint - Satellite

Library for obtaining the wind wave footprint of a given Tropical Cyclone, based on the work presented in *Cagigal et al., (Submitted)* <br>

**The model (Cagigal et al., Submitted) assumes a circular shape of the TC influence area and defines composites of significant wave height clustered as a function of minimum pressure, TC forward velocity and track latitude.** <br>

- The plot below serves as a summary of the model, in which we can observe the centroid of the TC track parameters (Pressure, Speed and Latitude) for the 49 clusters (*left*) and the composite of significant wave height associated to each cluster, represented by its 0.5 quantile (*right*).

![picture](resources/som_mean.png)


## Install
- - -

Install requirements. Navigate to the base root of [Wave Footprint - Satellite](./) and execute:

```

pip install -r requirements.txt

```

## Data
- - -

Data needed for running the codes can be found [here](https://unican-my.sharepoint.com/:f:/g/personal/cagigall_unican_es/EgZt8hGSqiZCtMCubbderM0Bve0qUusRPtvilfA4-Ylg-A?e=1F7JtQ)

## Contributors:

Laura Cagigal (cagigall@unican.es) <br>

```



